Your blockchain must have *all* of the following properties:

* It's a merkle tree, or a construct with equivalent properties.
* There is no single point of trust or authority; nodes are operated by different parties.
* Multiple 'forks' of the blockchain may exist - that is, nodes may disagree on what the full sequence of blocks looks like.
* In the case of such a fork, there must exist a deterministic consensus algorithm of some sort to decide what the "real" blockchain looks like (ie. which fork is "correct").
* The consensus algorithm must be executable with *only* the information contained in the blockchain (or its forks), and no external input (eg. no decisionmaking from a centralized 'trust node').

If your blockchain is missing *any* of the above properties, __it is not a blockchain, it is just a ledger.__